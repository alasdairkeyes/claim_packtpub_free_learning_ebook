# claim_packtpub_free_learning_ebook
Script to login to your Packtpub account and claim the free learning ebook


## NOTE: Not under active development

As of 2017-05-26, PacktPub have placed a reCaptcha on the free E-Learning
page making this script pretty much useless. There are projects to try 
and get around this but I feel that it's not worth trying to employ them
for this project.

Unfortunately, unless the reCaptcha is removed, this project is now dead.

Feel free to fork and adapt if you want to carry on using it.


## Installation
There's no installation, just a single script.



## Dependencies
- Perl v5.8
- LWP::UserAgent Perl module
- HTTP::Cookies Perl module

These will all most likely be installed on your Linux System already but if
not, will definitely be in your system's package repositories




## Usage
1. Get an account at Packtpub https://www.packtpub.com/
2. Add your account email and password into the claim_packtpub_fl_ebook.pl
script
3. Execute the script once a day by cron to have the day's ebook added to
your account



## I've found a problem
If you've found a problem, please report it



## Site:
https://gitlab.com/alasdairkeyes/claim_packtpub_free_learning_ebook



## Notes
Try it... go on, why not?



## License
- Released Under GPL Version 3 - See included LICENSE file



## Changelog
- 2017-03-07 :: 0.5     :: Fix redirect checks after purchase
                           Add in debug
- 2017-02-05 :: 0.4     :: Fix missing apostrophe
- 2017-01-12 :: 0.3     :: Print UTF-8 characters correctly
- 2016-12-18 :: 0.2     :: Display message when Packt have temporarily
                           stopped running the free ebook deal
- 2016-06-12 :: 0.1     :: First release



## Thanks
Thanks to Packtpub for providing the free learning books, please support
Packtpub and their authors by buying the occasional book too.



## Author
- Alasdair Keyes - https://akeyes.co.uk/
